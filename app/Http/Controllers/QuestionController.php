<?php

namespace App\Http\Controllers;

use App\Question;

use Illuminate\Http\Request;

use App\Http\Requests;

class QuestionController extends Controller
{
    /*
    For the question page
     */

    public function showForm()
    {
        return view('question');
    }


    public function submitForm(Request $request)
	{

    $this->validate($request, [
        'name' => 'required|max:255',
        'email' => 'required|max:255',
        'question' => 'required|max:255',
    ]);

	$question = new Question;

    $question->name = $request->name;
	$question->email = $request->email;
	$question->question = $request->question;
    $question->save();

    return redirect('/question');
	}
}
